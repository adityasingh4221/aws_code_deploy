<?php

use App\Http\Controllers\ServiceCategorysController;
use App\OfficeBuyer;
use App\ServiceCategory;
use Illuminate\Http\Request;
use Illuminate\Support\Facades\Route;

//auth group
Route::group([], function () {
    // get auth token, POST, /login
    Route::post('login', 'LoginController@login');
    // registers a new user, POST, /register // NEEDS TO ATTCH AN OFFICE BUYERS ON CREATION
    Route::post('register', 'RegisterController@register');
    // deletes the current users API token, POST, /logout
    Route::post('logout', 'LoginController@logout')->middleware('auth:api');
    //reset password logged in
    Route::middleware('auth:api')->post('reset_password', 'UserController@resetPassword');
    //request reset password
    Route::post('forgot_password', 'UserController@RequestPasswordReset');
    //reset password
    Route::post('reset_password_token', 'UserController@resetPasswordWithToken');
    //activate user
    Route::post('activate', 'UserController@Activate');
});

//account groups
Route::group([], function () {
    //get the account associated with this user, GET, /account
    Route::middleware('auth:api')->get('account', 'UserController@show');
    //update the account associated with this user, POST, /account
    Route::middleware('auth:api')->post('account', 'UserController@edit');
});

//services group
Route::group([], function () {
    //get all service categorys
    Route::get('services', 'ServiceCategoriesController@index');
    //get a service
    Route::get('service/{id}', 'ServiceCategoriesController@find');
    //get all service categorys
    Route::get('services/masters', 'ServiceCategoriesController@indexMasters');
    //returns all service categories that match any part of the search phrase
    Route::get('services/search', 'ServiceCategoriesController@search');

    //subscriptions
    Route::middleware('auth:api')->put('subscriptions/services/{service_category_id}', 'SubscriptionController@subscribe');
    Route::middleware('auth:api')->delete('subscriptions/services/{subscription_id}', 'SubscriptionController@unsubscribe');
    Route::middleware('auth:api')->get('subscriptions', 'SubscriptionController@showSubscriptions');
});

//partners group
Route::group(['prefix' => 'partners'], function () {
    //get partner details
    Route::get('/{id}', 'SellingPartnerController@show');
    //get partners reviews
    Route::get('/{id}/reviews', 'SellingPartnerController@showReviews');
    //get partners review summary
    Route::get('/{id}/reviews_summary', 'SellingPartnerController@showReviewsSummary');
    //returns all subscribed selling parters that match all of the question requirments
    Route::get('/services/{id}/search', 'ServiceCategoriesController@fullSearch');
});

//buyers group
Route::group([], function () {
    //get an office buyers profile
    Route::middleware('auth:api')->get('buyer/{id}', 'OfficeBuyerController@show');
});

//qwotes group
Route::group([], function () {
    //get current selling partners qwotes (office buyers who have shortlisted)
    Route::middleware('auth:api')->get('qwotes', 'ShortlistController@showQwotes');
    //get pending selling partners qwotes (office buyers who have shortlisted)
    Route::middleware('auth:api')->get('qwotes/pending', 'ShortlistController@showQwotesPending');
    //get requested selling partners qwotes (office buyers who have shortlisted)
    Route::middleware('auth:api')->get('qwotes/requested', 'ShortlistController@showQwotesRequested');
    //get completed selling partners qwotes (office buyers who have shortlisted)
    Route::middleware('auth:api')->get('qwotes/complete', 'ShortlistController@showQwotesComplete');
    //get completed selling partners qwotes (office buyers who have shortlisted)
    Route::middleware('auth:api')->get('qwotes/rejected', 'ShortlistController@showQwotesRejected');
    //get completed selling partners qwotes (office buyers who have shortlisted)
    Route::middleware('auth:api')->get('qwotes/{id}/reject', 'ShortlistController@rejectQwote');
    //get completed selling partners qwotes (office buyers who have shortlisted)
    Route::middleware('auth:api')->get('qwotes/{id}/complete', 'ShortlistController@completeQwote');
});

//chat group
Route::group(['prefix' => 'chats'], function () {
    //get current users chats
    Route::middleware('auth:api')->get('', 'ChatController@index');
    //get current specified chat
    Route::middleware('auth:api')->get('/{id}', 'ChatController@showMessages');
    //create new chat between current user and a list of other users
    Route::middleware('auth:api')->put('', 'ChatController@createChat');
    //join a specified chat
    Route::middleware('auth:api')->put('/{id}', 'ChatController@joinChat');

    //get current users chats
    //Route::middleware('auth:api')->get('/{id}/messages', 'ChatController@showMessages');
    //create new chat between current user and a list of other users
    Route::middleware('auth:api')->put('/{id}/messages', 'ChatController@postMessage');
    //mark all messges in a chat as seen
    Route::middleware('auth:api')->post('/{id}/seen', 'ChatController@seeChat');
    //mark a message as seen
    Route::middleware('auth:api')->post('/{chat_id}/messages/{id}/seen', 'ChatController@seeMessage');
});

//qwote_request group
Route::group(['prefix' => 'qwote_requests'], function() {
    //get all qwote requests
    Route::middleware('auth:api')->get('', 'QwoteRequestController@index');
    //create a qwote request
    Route::middleware('auth:api')->put('', 'QwoteRequestController@store');
    //get a specified qwote request
    Route::middleware('auth:api')->get('/{id}', 'QwoteRequestController@show');
    //update a qwote request
    Route::middleware('auth:api')->post('/{id}', 'QwoteRequestController@update');

    //shortlist
    Route::middleware('auth:api')->put('/{qwote_request_id}/shortlist/{subscription_id}', 'ShortlistController@shortlist');
    Route::middleware('auth:api')->delete('/shortlist/{shortlist_id}', 'ShortlistController@unshortlist');
    Route::middleware('auth:api')->delete('/{qwote_request_id}/shortlist/{subscription_id}', 'ShortlistController@deshortlist');
    Route::middleware('auth:api')->post('/shortlist/{shortlist_id}', 'ShortlistController@update');
    //Route::middleware('auth:api')->get('/{qwote_request_id}/shortlist', 'ShortlistController@showShortlist');
});

//companies house API
Route::group([], function() {
    //search companies house 
    Route::middleware('auth:api')->get('/companies_house/search', 'CompaniesHouseController@searchCompanies');
    //get a companies info from Companies Hosue
    Route::middleware('auth:api')->get('/companies_house/get_info', 'CompaniesHouseController@getCompanyInfo');
});

//portfolio
Route::group([], function() {
    //get partners portfolio
    Route::get('/partners/{id}/portfolio', 'PortfolioController@index');
    //create portfolio entry
    Route::middleware('auth:api')->put('/portfolio', 'PortfolioController@store');
    //update portfolio entry
    Route::middleware('auth:api')->post('/portfolio/{id}', 'PortfolioController@update');
    //delete portfolio entry
    Route::middleware('auth:api')->delete('/portfolio/{id}', 'PortfolioController@destroy');

});

//notifications
Route::group(['prefix' => 'notifictions'], function(){
    //get all notifications
    Route::middleware('auth:api')->get('', 'NotificationController@index');
    //get unseen notificaitons
    Route::middleware('auth:api')->get('/unseeen', 'NotificationController@indexUnseen');
    //mark all notificatiosn as seen
    Route::middleware('auth:api')->post('/seen', 'NotificationController@seeAll');
    //mark a notificatio as seen
    Route::middleware('auth:api')->post('/{id}/seen', 'NotificationController@see');
});

//user stories
Route::get('stories', 'UsersStoriesController@index');
//get a buyers info
Route::get('buyers/{id}', 'OfficeBuyerController@find');
//subscribe to newsletter
Route::put('newsletter/subscribe', 'NewsletterSubscriptionsController@store');
