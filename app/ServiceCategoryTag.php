<?php

namespace App;

use Illuminate\Database\Eloquent\Model;

class ServiceCategoryTag extends Model
{
    protected $fillabale = [
        'search_tag_id', 'service_category_id',
    ];
}
