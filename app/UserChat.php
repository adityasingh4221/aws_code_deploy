<?php

namespace App;

use Illuminate\Database\Eloquent\Model;

class UserChat extends Model
{

    protected $fillable = [
        'chat_id', 'user_id', 'last_time_seen'
    ];
    
    public function user()
    {
        return $this->belongsTo('App\User', 'user_id', 'id');
    }

    public function chat()
    {
        return $this->belongsTo('App\Chat', 'chat_id', 'id');
    }
}
