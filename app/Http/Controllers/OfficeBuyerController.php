<?php

namespace App\Http\Controllers;

use App\OfficeBuyer;
use Illuminate\Http\Request;

class OfficeBuyerController extends Controller
{
    public function find(Request $request)
    {
        return OfficeBuyer::where('id', $request->id)->firstOrFail();
    }
}
