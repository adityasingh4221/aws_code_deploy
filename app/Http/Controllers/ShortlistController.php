<?php

namespace App\Http\Controllers;

use App\QwoteRequest;
use Illuminate\Http\Request;
use App\Shortlist;
use Illuminate\Database\Eloquent\Builder;

class ShortlistController extends Controller
{
    public function shortlist(Request $request)
    {
        $request->validate(
            [
                //TODO
                //test that we are a office buyer
                //test this doesn't exist or add migration
                //'subscription_id' => ['required']
                //test we own this QR
            ]
        );

        //test we own this QR
        $qr = QwoteRequest::where('id',$request->qwote_request_id)->where('office_buyer_id', $request->user()->office_buyer_id)->firstOrFail();

        if($qr)
        {
            $sh = shortlist::where('qwote_request_id', $qr->id)
            ->where('subscription_id',$request->subscription_id)->first();
            if(!$sh)
                return Shortlist::create(
                    [
                        'subscription_id' => $request->subscription_id,
                        'qwote_request_id' => $request->qwote_request_id
                    ]
                );
            else {
                return response()->json([
                    'message' => 'Shortlist entry already exists.'
                ], 409);
            }
        }

        return response()->json([
            'message' => 'We can\'t find your qwote request with that id.'
        ], 404);
    }

    public function deshortlist(Request $request)
    {
     //test we own this QR
     $qr = QwoteRequest::where('id',$request->qwote_request_id)->where('office_buyer_id', $request->user()->office_buyer_id)->firstOrFail();   
     if($qr)
        {
            $sh = shortlist::where('qwote_request_id', $qr->id)
            ->where('subscription_id',$request->subscription_id)->first();
            if($sh)
                return Shortlist::destroy($sh->id);
            else {
                return response()->json([
                    'message' => 'We can\'t find your shortlist with that id.'
                ], 404);
            }
        }
        
        return response()->json([
            'message' => 'We can\'t find your qwote request with that id.'
        ], 404);

    }

    public function unshortlist(Request $request)
    {
        $request->validate(
            [
                //TODO
                //test that we are a office buyer
                //test this does exist
                //'shortlist_id' => ['required']
                //test we own this QR
            ]
        );
        //test we own this QR
        
        //if($qr)
        //{
            return Shortlist::destroy($request->shortlist_id);
        //}
        
        return response()->json([
            'message' => 'We can\'t find your qwote request with that id.'
        ], 404);

    }

    public function update(Request $request, Shortlist $shortlist)
    {
        $inputs = $request->all();
        $qr = QwoteRequest::where('office_buyer_id', request()->user()->office_buyer_id)->whereHas('shortlist', function (Builder $query) use ($request) {
            $query->where('id', $request->shortlist_id);
        }, '>=', 1)->get();

        if($qr && $qr->count() >= 1)
            return Shortlist::where('id', $request->shortlist_id)
                ->update($inputs);
        else
        return response()->json([
            'message' => 'We can\'t find your shorlist with that id.'
        ], 404);

    }

    public function showShortlist(Request $request)
    {
        //test we own this QR
        $qr = QwoteRequest::where('office_buyer_id', $request->user()->office_buyer_id)
        ->where('id',$request->qwote_request_id)->firstOrFail();

        if($qr)
        {
            return Shortlist::with('subscription', 'subscription.selling_partner')
            ->where('qwote_request_id', $request->qwote_request_id)->get();
        }
        
        return response()->json([
            'message' => 'We can\'t find your qwote request with that id.'
        ], 404);
    }

    public function showQwotes(Request $request)
    {
        return Shortlist::whereHas('subscription',  function ($query) use ($request) {
            $query->where('selling_partner_id', $request->user()->selling_partner_id);
        })->with('office_buyer')->get();
    }

    public function showQwotesPending(Request $request)
    {
        return Shortlist::whereHas('subscription',  function ($query) use ($request) {
            $query->where('selling_partner_id', $request->user()->selling_partner_id);
            $query->where('status', '=', 'pending');
        })->with('office_buyer')->get();
    }

    public function showQwotesRequested(Request $request)
    {
        return Shortlist::whereHas('subscription',  function ($query) use ($request) {
            $query->where('selling_partner_id', $request->user()->selling_partner_id);
            $query->where('status', '=', 'requested');
        })->with('office_buyer')->get();
    }

    public function showQwotesComplete(Request $request)
    {
        return Shortlist::whereHas('subscription',  function ($query) use ($request) {
            $query->where('selling_partner_id', $request->user()->selling_partner_id);
            $query->where('status', '=', 'complete');
        })->with('office_buyer')->get();
    }

    public function showQwotesRejected(Request $request)
    {
        return Shortlist::whereHas('subscription',  function ($query) use ($request) {
            $query->where('selling_partner_id', $request->user()->selling_partner_id);
            $query->where('status', '=', 'rejected');
        })->with('office_buyer')->get();
    }

    public function rejectQwote(Request $request)
    {
        $shortlist = Shortlist::find($request->id)->firstOrFail();

        if(!$shortlist) {
            return response()->json([
                'message' => 'We can\'t find a qwote with that id.'
            ], 404);
        }

        if(!$shortlist->selling_partner_id == $request->user()->selling_partner_id)
        {
            return response()->json([
                'message' => 'Unautherized.'
            ], 501);
        }

        $shortlist->status = 'rejected';
        $shortlist->save();

        return $shortlist->id;
    }

    public function completeQwote(Request $request)
    {
        $shortlist = Shortlist::find($request->id)->firstOrFail();

        if(!$shortlist) {
            return response()->json([
                'message' => 'We can\'t find a qwote with that id.'
            ], 404);
        }

        if(!$shortlist->selling_partner_id == $request->user()->selling_partner_id)
        {
            return response()->json([
                'message' => 'Unautherized.'
            ], 501);
        }

        $shortlist->status = 'complete';
        $shortlist->save();

        return $shortlist->id;
    }


}
