<?php

namespace App\Http\Controllers;

use App\SellingPartner;
use App\Shortlist;
use Illuminate\Http\Request;
use \stdClass;

class SellingPartnerController extends Controller
{
    public function show(Request $request) 
    {
        return SellingPartner::find(request()->route('id'));
    }

    public function showReviews (Request $request)
    {
        $reviews = Shortlist::whereHas('subscription',  function($query) use ($request){
            $query->where('selling_partner_id', $request->id);
        })->with('office_buyer')->get();

        $processed_reviews = array();

        $count = count(json_decode($reviews[0]->review));

        $review_totals = array_fill(0, $count, 0);
        $review_lables = array_fill(0, $count, 'Empty');
        
        foreach($reviews as $review)
        {
            $review->chart = $this->reviewToChart($review->review);

            $review_total = 0;

            $review_params_count = count($review->chart);

            for ($counter = 0; $counter < $review_params_count; $counter++) {
                $review_lables[$counter] = $review->chart[$counter]->label;
                $review_total += $review->chart[$counter]->value;
                $review_totals[$counter] += $review->chart[$counter]->value;
            }

            $review->star_rating = $review_total/($review_params_count * 10) ;

            array_push($processed_reviews, $review);
        }

        $review_counter = count($processed_reviews);

        $star_rating = 0;

        $over_chart = array();

        for ($counter = 0; $counter < count($review_totals); $counter++) {
            $processed = new stdClass();

            $processed->label = $review_lables[$counter];
            $processed->value = $review_totals[$counter] / $review_counter;

            $star_rating += $processed->value;

            array_push($over_chart, $processed);
        }

        $review_over = new stdClass();
        $review_over->review_count = $review_counter;
        $review_over->star_rating = $star_rating/(count($review_totals) * 10);

        $review_over->chart = $over_chart;
        $review_over->reviews = $processed_reviews;

        return json_encode($review_over);
    }

    public function showReviewsSummary (Request $request)
    {
        $reviews = Shortlist::whereHas('subscription',  function($query) use ($request){
            $query->where('selling_partner_id', $request->id);
        })->with('office_buyer')->get();

        $processed_reviews = array();

        $count = count(json_decode($reviews[0]->review));

        $review_totals = array_fill(0, $count, 0);
        $review_lables = array_fill(0, $count, 'Empty');
        
        foreach($reviews as $review)
        {
            $review->chart = $this->reviewToChart($review->review);

            $review_total = 0;

            $review_params_count = count($review->chart);

            for ($counter = 0; $counter < $review_params_count; $counter++) {
                $review_lables[$counter] = $review->chart[$counter]->label;
                $review_total += $review->chart[$counter]->value;
                $review_totals[$counter] += $review->chart[$counter]->value;
            }

            $review->star_rating = $review_total/($review_params_count * 10) ;

            array_push($processed_reviews, $review);
        }

        $review_counter = count($processed_reviews);

        $star_rating = 0;

        $over_chart = array();

        for ($counter = 0; $counter < count($review_totals); $counter++) {
            $processed = new stdClass();

            $processed->label = $review_lables[$counter];
            $processed->value = $review_totals[$counter] / $review_counter;

            $star_rating += $processed->value;

            array_push($over_chart, $processed);
        }

        $review_over = new stdClass();
        $review_over->review_count = $review_counter;
        $review_over->star_rating = $star_rating/(count($review_totals) * 10);

        $review_over->chart = $over_chart;
        //$review_over->reviews = $processed_reviews;

        return json_encode($review_over);
    }

    public function reviewToChart($review)
    {
        $json = json_decode($review);

        $processed_review = array();

        foreach($json as $part)
        {
            $part = json_encode($part);

            $first_quote = strpos($part, '"') + 1;
            $last_quote = strrpos($part, '"');
            $length = $last_quote - $first_quote;
            
            $colon_index = strpos($part, ':') + 1;
            $brace_index = strpos($part, '}');

            $label = substr($part, $first_quote, $length);

            $value = substr($part, $colon_index, $brace_index - $colon_index);

            $processed = new stdClass();

            $processed->label = $label;
            $processed->value = $value;

            array_push($processed_review, $processed);
        }

        return $processed_review;
    }
}
