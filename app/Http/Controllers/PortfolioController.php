<?php

namespace App\Http\Controllers;

use App\portfolio;
use Illuminate\Http\Request;

class PortfolioController extends Controller
{
    /**
     * Display a listing of the resource.
     *
     * @return \Illuminate\Http\Response
     */
    public function index(Request $request)
    {
        return Portfolio::where('selling_partner_id', $request->id)->get();
    }

    /**
     * Show the form for creating a new resource.
     *
     * @return \Illuminate\Http\Response
     */
    public function create()
    {
        
    }

    /**
     * Store a newly created resource in storage.
     *
     * @param  \Illuminate\Http\Request  $request
     * @return \Illuminate\Http\Response
     */
    public function store(Request $request)
    {
        $request->validate([
            'title' => ['required'],
            'image_url' => ['required'],
            'text' => ['required'],
        ]);

        if($request->user()->selling_partner_id)
        {
            return Portfolio::create([
                'selling_partner_id' => $request->user()->selling_partner_id,
                'title' => $request->title,
                'image_url' => $request->image_url,
                'text' => $request->text
            ]);
        }
        else
        {
            return response()->json([
                'message' => 'You do not have permission to access this feature.'
            ], 501);
        }
    }

    /**
     * Display the specified resource.
     *
     * @param  \App\portfolio  $portfolio
     * @return \Illuminate\Http\Response
     */
    public function show(portfolio $portfolio)
    {
        //
    }

    /**
     * Show the form for editing the specified resource.
     *
     * @param  \App\portfolio  $portfolio
     * @return \Illuminate\Http\Response
     */
    public function edit(portfolio $portfolio)
    {
        //
    }

    /**
     * Update the specified resource in storage.
     *
     * @param  \Illuminate\Http\Request  $request
     * @param  \App\portfolio  $portfolio
     * @return \Illuminate\Http\Response
     */
    public function update(Request $request, portfolio $portfolio)
    {
        $inputs = $request->all();
        $p = portfolio::where('id', $request->id)->where('selling_partner_id', $request->user()->selling_partner_id)->update($inputs);

        if($p)
            return $p;

        return response()->json([
            'message' => 'We can\'t find the specidifed portfolio.'
        ], 404);
    }

    /**
     * Remove the specified resource from storage.
     *
     * @param  \App\portfolio  $portfolio
     * @return \Illuminate\Http\Response
     */
    public function destroy(Request $portfolio)
    {
        $p = portfolio::where('id', $portfolio->id)->where('selling_partner_id', request()->user()->selling_partner_id)->firstOrFail();
        if($p)
            return portfolio::destroy($p->id);
        return response()->json([
            'message' => 'We can\'t find the specidifed portfolio.'
        ], 404);
    }
}
