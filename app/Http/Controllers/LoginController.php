<?php

/**
 * LoginController File Doc Comment
 *
 * @category File
 * @package  Controllers
 * @author   Display Name <brad@qwoteme.com>
 */

namespace App\Http\Controllers;

use App\User;
use Illuminate\Http\Request;
use Illuminate\Support\Facades\Hash;
use Illuminate\Validation\ValidationException;

/**
 * LoginController Class Doc Comment
 *
 * @category Class
 * @package  Controllers
 * @author   Display Name <brad@qwoteme.com>
 */
class LoginController extends Controller
{
    /**
     *  Authenticate a user returning thier API Token
     * 
     * @param Request $request The HTTP request
     *
     * @return APIToken
     */
    function login(Request $request)
    {
        $request->validate(
            [
                'email' => ['required', 'email'],
                'password' => ['required']
            ]
        );

        $user = User::where('email', $request->email)->first();

        //if user not found or the request password and user password does not match
        if (!$user || !Hash::check($request->password, $user->password)) {
            throw ValidationException::withMessages(
                [
                    'email' => ['The provided credentials are incorrect']
                ]
            );
        }

        return $user->createToken('Auth Token')->accessToken;
    }
    /**
     *  Deactivate the current API Token for this user
     * 
     * @param Request $request The HTTP request
     * 
     * @return null
     */
    function logout(Request $request)
    {
        $request->user()->tokens()->delete();
    }
}
