<?php

namespace App\Http\Controllers;

use App\UsersStories;
use Illuminate\Http\Request;

class UsersStoriesController extends Controller
{
    /**
     * Display a listing of the resource.
     *
     * @return \Illuminate\Http\Response
     */
    public function index()
    {
        return UsersStories::all();
    }

    /**
     * Show the form for creating a new resource.
     *
     * @return \Illuminate\Http\Response
     */
    public function create()
    {
        //
    }

    /**
     * Store a newly created resource in storage.
     *
     * @param  \Illuminate\Http\Request  $request
     * @return \Illuminate\Http\Response
     */
    public function store(Request $request)
    {
        //
    }

    /**
     * Display the specified resource.
     *
     * @param  \App\UsersStories  $usersStories
     * @return \Illuminate\Http\Response
     */
    public function show(UsersStories $usersStories)
    {
        //
    }

    /**
     * Show the form for editing the specified resource.
     *
     * @param  \App\UsersStories  $usersStories
     * @return \Illuminate\Http\Response
     */
    public function edit(UsersStories $usersStories)
    {
        //
    }

    /**
     * Update the specified resource in storage.
     *
     * @param  \Illuminate\Http\Request  $request
     * @param  \App\UsersStories  $usersStories
     * @return \Illuminate\Http\Response
     */
    public function update(Request $request, UsersStories $usersStories)
    {
        //
    }

    /**
     * Remove the specified resource from storage.
     *
     * @param  \App\UsersStories  $usersStories
     * @return \Illuminate\Http\Response
     */
    public function destroy(UsersStories $usersStories)
    {
        //
    }
}
