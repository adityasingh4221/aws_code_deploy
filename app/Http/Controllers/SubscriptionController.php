<?php

namespace App\Http\Controllers;

use App\Subscription;
use Illuminate\Http\Request;

class SubscriptionController extends Controller
{
    public function subscribe(Request $request)
    {
        $request->validate(
            [
                'requirements_provided' => ['required'],
            ]
        );

        return Subscription::create(
            [
                'service_category_id' => $request->service_category_id,
                'selling_partner_id' => $request->user()->selling_partner_id,
                'requirements_provided' => $request->requirements_provided,
                'feature_list' => '[]',
            ]
        );
    }
    public function unsubscribe(Request $request)
    {
        $request->validate(
            [
                //TODO
                //test that we are an selling partner
                //test this doesn't exist or add migration
                //'subscription_id' => ['required']
            ]
        );

        return Subscription::destroy($request->subscription_id);
    }

    public function showSubscriptions(Request $request)
    {
        return Subscription::with('service_category')
            ->where('selling_partner_id', request()->user()->selling_partner_id)->get();
    }
}
