<?php

namespace App\Http\Controllers;

use App\Notification;
use Illuminate\Http\Request;

class NotificationController extends Controller
{
    /**
     * Display a listing of the resource.
     *
     * @return \Illuminate\Http\Response
     */
    public function index(Request $request)
    {
        return Notification::where('user_id', $request->user()->id)->get();
    }

    public function indexUnseen(Request $request)
    {
        return Notification::where('user_id', $request->user()->id)->where('time_seen', null)->get();
    }

    public function seeAll(Request $request)
    {
        $notifications = Notification::where('user_id', $request->user()->id)->where('time_seen', null)->get();

        foreach($notifications as $notification)
        {
            $notification->time_seen = now();
            $notification->save();
        }
    }

    public function see(Request $request)
    {
        $notification = Notification::where('id', $request->id)->where('user_id', $request->user()->id)->firstOrFail();

        if($notification->time_seen == null)
        {
            $notification->time_seen = now();
            $notification->save();
        }

    }

    /**
     * Show the form for creating a new resource.
     *
     * @return \Illuminate\Http\Response
     */
    public function create()
    {
        //
    }

    /**
     * Store a newly created resource in storage.
     *
     * @param  \Illuminate\Http\Request  $request
     * @return \Illuminate\Http\Response
     */
    public function store(Request $request)
    {
        //
    }

    /**
     * Display the specified resource.
     *
     * @param  \App\Notification  $notification
     * @return \Illuminate\Http\Response
     */
    public function show(Notification $notification)
    {
        //
    }

    /**
     * Show the form for editing the specified resource.
     *
     * @param  \App\Notification  $notification
     * @return \Illuminate\Http\Response
     */
    public function edit(Notification $notification)
    {
        //
    }

    /**
     * Update the specified resource in storage.
     *
     * @param  \Illuminate\Http\Request  $request
     * @param  \App\Notification  $notification
     * @return \Illuminate\Http\Response
     */
    public function update(Request $request, Notification $notification)
    {
        //
    }

    /**
     * Remove the specified resource from storage.
     *
     * @param  \App\Notification  $notification
     * @return \Illuminate\Http\Response
     */
    public function destroy(Notification $notification)
    {
        //
    }
}
