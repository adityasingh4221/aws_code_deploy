<?php

/**
 * RegisterController File Doc Comment
 *
 * @category File
 * @package  Controllers
 * @author   Display Name <brad@qwoteme.com>
 */

namespace App\Http\Controllers;

use App\Notifications\MailActivateOnSignup;
use App\OfficeBuyer;
use App\SellingPartner;
use App\User;
use Illuminate\Http\Request;
use Illuminate\Support\Facades\Hash;

/**
 * RegisterController Class Doc Comment
 *
 * @category Class
 * @package  Controllers
 * @author   Display Name <brad@qwoteme.com>
 */
class RegisterController extends Controller
{
    /**
     *  Updates the Office Buyer account for the current user if exists
     *  Uses the currrent request is input
     * 
     * @param Request $request The HTTP request
     *
     * @return null
     */
    public function register(Request $request)
    {
        $user = null;
        
        if ($request->account_type && $request->account_type == ('selling_partner')) {

            $request->validate(
                [
                    'email' => ['required', 'email', 'unique:users'],
                    'password' => ['required', "min:8", 'confirmed'],

                    'company_house_no' => ['required', 'unique:selling_partners'],
                    'company_name' => ['required', 'unique:selling_partners'],
                    'company_logo' => ['required', 'url', 'unique:selling_partners'],
                    'company_description' => ['required'],
                    'head_office_address' => ['required'],
                ]
            );

            $user = User::create(
                [
                    'name' => $request->company_name,
                    'email' => $request->email,
                    'password' => Hash::make($request->password),
                    'activation_token' => str_random(60),
                    'account_type' => 'selling_partner',
                    'selling_partner_id' => SellingPartner::create(
                        [
                            'company_house_no' => $request->company_house_no,
                            'company_name' => $request->company_name,
                            'company_logo' => $request->company_logo,
                            'carousel_media' => $request->carousel_media,
                            'company_description' => $request->company_description,
                            'head_office_address' => $request->head_office_address,
                            'director_name' => $request->director_name,
                            'director_contact' => $request->director_contact,
                            'account_name' => $request->account_name,
                            'account_contact' => $request->account_contact,
                            'legal_name' => $request->legal_name,
                            'legal_contact' => $request->legal_contact,
                            'number_of_employees' => $request->number_of_employees,
                            'turnover_pa' => $request->turnover_pa,
                            'rep_first_name' => $request->rep_first_name,
                            'rep_last_name' => $request->rep_last_name,
                            'rep_job_title' => $request->rep_job_title,
                            'rep_email' => $request->email,
                            'rep_contact' => $request->rep_contact,
                            'verified_partner' => false,
                        ]
                    )->id,
                ]
            );
        } else {

            $request->validate(
                [
                    'email' => ['required', 'email', 'unique:users'],
                    'password' => ['required', "min:8", 'confirmed'],

                    'first_name' => ['required'],
                    'contact' => ['required'],
                    'office_address' => ['required'],
                ]
            );

            $user = User::create(
                [
                    'name' => $request->first_name . '_' . $request->last_name,
                    'email' => $request->email,
                    'password' => Hash::make($request->password),
                    'activation_token' => str_random(60),
                    'account_type' => 'office_buyer',
                    'office_buyer_id' => OfficeBuyer::create(
                        [
                            'first_name' => $request->first_name,
                            'last_name' => $request->last_name,
                            'contact' => $request->contact,
                            'office_address' => $request->office_address,
                            'number_of_employees' => $request->number_of_employees,
                            'turnover_pa' => $request->turnover_pa,
                        ]
                    )->id,
                ]
            );
        }

        $user->notify(new MailActivateOnSignup($user));

    }
}
