<?php

namespace App\Http\Controllers;

use App\Chat;
use App\ChatMessage;
use App\User;
use App\UserChat;
use GuzzleHttp\Psr7\Message;
use Illuminate\Database\Eloquent\Builder;
use Illuminate\Http\Request;

class ChatController extends Controller
{
    public function index(Request $request)
    {
        //return Chat::where('id', 1)->with('users')->get();

        $chats = Chat::whereHas('users', function (Builder $query) use ($request) {
            $query->where('users.id', '=', $request->user()->id);
        })->with('users')->get();

        foreach($chats as $chat)
        {
            $lastMessage = ChatMessage::where('chat_id', $chat->id)->orderBy('created_at')->firstOrFail();
            $chat->last_message = $lastMessage;
        }

        return $chats;
    }

    public function find(Request $request)
    {
        $userChat = UserChat::where('chat_id', $request->id)->where('user_id', $request->user()->id)->firtOrFail();
        if($userChat)
        {
            return Chat::find($request->id)->whereHas('users', function (Builder $query) use ($request) {
                $query->where('users.id', '=', $request->user()->id);
            })->with('users')->get();
        }
        else
        {
            return response()->json([
                'message' => 'We can\'t find the chat specified!.'
            ], 404);
        }
    }

    public function createChat(Request $request)
    {
        if($request->selling_partner_id)
            $other_user = User::where('selling_partner_id', $request->partner_id)->firstOrFail();
        else if($request->office_buyer_id)
            $other_user = User::where('office_buyer_id', $request->partner_id)->firstOrFail();

        //TEST THIS CHAT EXISTS
        $existing_chat = Chat::whereHas('user',  function ($query) use ($request, $other_user) {
                $query->where('user', $request->user()->id);
                $query->where('user', $other_user->id);
            })->get();

        if(!$existing_chat)
        {
            $chat = Chat::create();

            UserChat::create(
                [
                    'chat_id' => $chat->id,
                    'user_id' => $request->user()->id,
                ]
            );

            UserChat::create(
                [
                    'chat_id' => $chat->id,
                    'user_id' => $other_user->id,
                ]
            );

            return $chat;
        }
        else
        {
            return $existing_chat;
        }
        
    }

    public function joinChat(Request $request)
    {
        $request->validate(
            [
                'chat_id' => ['required'],
            ]
        );

        $chat = Chat::find($request->chat_id);

        if($chat->id == $request->chat_id)
            return UserChat::create(
                [
                    'chat_id' => $chat->id,
                    'user_id' => request()->user()->id,
                ]
            
            );
        return 'error';
    }

    public function leaveChat(Request $request)
    {
        $request->validate(
            [
                'user_chat_id' => ['required'],
            ]
        );
        return Chat::destroy($request->user_chat_id);
    }
    //TODO::check we are in the chat first!!
    public function showMessages(Request $request)
    {
        $userChat = UserChat::where('chat_id', $request->id)->where('user_id', $request->user()->id)->firtOrFail();

        if($userChat)
        {
            $chat = Chat::where('id', $request->id)->with('messages.user')->firstOrFail();

            foreach($chat->messages as $message)
            {
                $message->seen = $message->wasSeen();
            }
            
            return $chat;
        }
        else
        {
            return response()->json([
                'message' => 'We can\'t find the chat specified!.'
            ], 404);
        }
    }

    public function seeChat(Request $request)
    {
        $userChat = UserChat::where('chat_id', $request->id)->where('user_id', $request->user()->id)->firtOrFail();

        if($userChat)
        {
            $chat = Chat::where('id', $request->id)->with('messages.user')->firstOrFail();

            foreach($chat->messages as $message)
            {
                if($message->user_id != $request->user()->id)
                {
                    $message->time_seen = now();
                    $message->save();
                }

                $message->seen = $message->wasSeen();
            }

            $userChat->last_time_seen = now();
            $userChat->save();
            
            return $chat;
        }
        else
        {
            return response()->json([
                'message' => 'We can\'t find the chat specified!.'
            ], 404);
        }
    }

    public function seeMessage(Request $request)
    {
        $userChat = UserChat::where('chat_id', $request->chat_id)->where('user_id', $request->user()->id)->firtOrFail();

        if($userChat)
        {
            $chat_message = ChatMessage::where('chat_id', $request->chat_id)->where('id', $request->id)->firstOrFail();
            $chat_message->time_seen = now();
        }
        else
        {
            return response()->json([
                'message' => 'We can\'t find the chat specified!.'
            ], 404);
        }
    }

    public function postMessage(Request $request)
    {
        $request->validate(
            [
                //'id' => ['required'],
                'text' => ['required'],
            ]
        );

        return ChatMessage::create(
            [
                'user_id' => $request->user()->id,
                'chat_id'=> $request->id,
                'text' => $request->text,
            ]
        );
    }
}
