<?php

/**
 * Users File Doc Comment
 *
 * @category File
 * @package  Controllers
 * @author   Display Name <brad@qwoteme.com>
 */

namespace App\Http\Controllers;

use App\Notifications\MailResetPasswordNotification;
use App\OfficeBuyer;
use App\SellingPartner;
use Illuminate\Http\Request;
use App\User;
use App\PasswordReset;
use Illuminate\Support\Facades\Hash;
use Illuminate\Support\Facades\Storage;

/**
 * Users Class Doc Comment
 *
 * @category Class
 * @package  Controllers
 * @author   Display Name <brad@qwoteme.com>
 */
class UserController extends Controller
{
    /**
     * Display the specificed resource
     *
     * @return Account for the current user can be an OfficeBuyer or SellingPartner
     */
    function show()
    {
        if (request()->user()->account_type && request()->user()->account_type == 'selling_partner') {
            return User::with('selling_partner')->where('id', request()->user()->id)->firstOrFail();
        } else {
            return User::with('office_buyer')->where('id', request()->user()->id)->firstOrFail();
        }
    }

    /**
     * Update the specificed resource
     * 
     * @return Account for the current user can be an OfficeBuyer or SellingPartner
     */
    function edit()
    {
        $inputs = request()->all();
        if (request()->user()->account_type && request()->user()->account_type == 'selling_partner') {
            return SellingPartner::where('id', request()->user()->selling_partner_id)
                ->update($inputs);
        } else {
            return OfficeBuyer::where('id', request()->user()->office_buyer_id)
                ->update($inputs);
        }
    }

    function showActivity(Request $request)
    {
        return 'NOT IMPLEMENTED';
    }

    function resetPassword(Request $request)
    {
        $request->validate([
            'password' => 'required|string|confirmed|min:8',
        ]);

        $user = request()->user();

        $user->password = Hash::make($request->password);
        $user->save();
    }

    function RequestPasswordReset(Request $request)
    {
        $request->validate([
            'email' => 'required|string|email',
        ]);

        $user = User::where('email', $request->email)->first();

        if (!$user) {
            return response()->json([
                'message' => 'We can\'t find a user with that e-mail address.'
            ], 404);
        }

        $reset = PasswordReset::updateOrCreate(
            ['email' => $user->email],
            [
                'email' => $user->email,
                'token' => str_random(60)
            ]
        );

        if ($user && $reset)
            $user->notify(
                new MailResetPasswordNotification($reset->token)
            );
        return response()->json([
            'message' => 'We have e-mailed your password reset link!'
        ]);
    }

    function resetPasswordWithToken(Request $request)
    {

        $request->validate([
            'password' => 'required|string|confirmed|min:8',
            'token' => 'required|string'
        ]);

        $token = $request->token;

        $reset = PasswordReset::where('token', $token)->first();

        if (!$reset) {
            return response()->json([
                'message' => 'This password reset token is invalid.'
            ], 404);
        }

        $user = User::where('email', $reset->email)->first();

        if (!$user) {
            return response()->json([
                'message' => 'We can\'t find a user with that e-mail address.'
            ], 404);
        }

        $user->password = Hash::make($request->password);
        $user->save();

        $reset->delete();
    }

    function Activate(Request $request)
    {
        $user = User::where('activation_token', $request->activation_token)->first();

        if (!$user) {
            return response()->json([
                'message' => 'We can\'t find a user with that activation token.'
            ], 404);
        }

        $user->active = true;
        $user->save();

        return $user->id;
    }

    public function store(Request $request)
    {
        if ($request->hasFile('profile_image')) {

            //get filename with extension
            $filenamewithextension = $request->file('profile_image')->getClientOriginalName();

            //get filename without extension
            $filename = pathinfo($filenamewithextension, PATHINFO_FILENAME);

            //get file extension
            $extension = $request->file('profile_image')->getClientOriginalExtension();

            //filename to store
            $filenametostore = $filename . '_' . time() . '.' . $extension;

            //Upload File to s3
            Storage::disk('s3')->put($filenametostore, fopen($request->file('profile_image'), 'r+'), 'public');

            //Store $filenametostore in the database
        }
    }
}
