<?php

namespace App\Http\Controllers;

use App\NewsletterSubscription;
use Illuminate\Http\Request;

class NewsletterSubscriptionsController extends Controller
{
    /**
     * Display a listing of the resource.
     *
     * @return \Illuminate\Http\Response
     */
    public function index()
    {
        //
    }

    /**
     * Show the form for creating a new resource.
     *
     * @return \Illuminate\Http\Response
     */
    public function create()
    {
        //
    }

    /**
     * Store a newly created resource in storage.
     *
     * @param  \Illuminate\Http\Request  $request
     * @return \Illuminate\Http\Response
     */
    public function store(Request $request)
    {
        $request->validate(
            [
                'email' => ['required', 'email',]
            ]
        );
        return NewsletterSubscription::create(['email' => $request->email,]);
    }

    /**
     * Display the specified resource.
     *
     * @param  \App\NewsletterSubscription  $NewsletterSubscription
     * @return \Illuminate\Http\Response
     */
    public function show(NewsletterSubscription $NewsletterSubscription)
    {
        //
    }

    /**
     * Show the form for editing the specified resource.
     *
     * @param  \App\NewsletterSubscription  $NewsletterSubscription
     * @return \Illuminate\Http\Response
     */
    public function edit(NewsletterSubscription $NewsletterSubscription)
    {
        //
    }

    /**
     * Update the specified resource in storage.
     *
     * @param  \Illuminate\Http\Request  $request
     * @param  \App\NewsletterSubscription  $NewsletterSubscription
     * @return \Illuminate\Http\Response
     */
    public function update(Request $request, NewsletterSubscription $NewsletterSubscription)
    {
        //
    }

    /**
     * Remove the specified resource from storage.
     *
     * @param  \App\NewsletterSubscription  $NewsletterSubscription
     * @return \Illuminate\Http\Response
     */
    public function destroy(NewsletterSubscription $NewsletterSubscription)
    {
        //
    }
}
