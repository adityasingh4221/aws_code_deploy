<?php

/**
 * ServiceCategoriesController File Doc Comment
 *
 * @category File
 * @package  Controllers
 * @author   Display Name <brad@qwoteme.com>
 */

namespace App\Http\Controllers;

use App\ServiceCategory;
use App\ServiceCategoryTag;
use App\Shortlist;
use App\Subscription;
use Illuminate\Database\Eloquent\Builder;
use Illuminate\Http\Request;
use stdClass;

/**
 * ServiceCategoriesController Class Doc Comment
 *
 * @category Class
 * @package  Controllers
 * @author   Display Name <brad@qwoteme.com>
 */
class ServicecategoriesController extends Controller
{
    /**
     * Display a listing of the resource.
     *
     * @return \Illuminate\Http\Response
     */
    public function index()
    {
        return ServiceCategory::all();
    }

    /**
     * Display a listing of the resource.
     *
     * @return \Illuminate\Http\Response
     */
    public function indexMasters()
    {
        return ServiceCategory::select('master_category')
            ->distinct('master_category')->get();
    }

    /**
     * Store a newly created resource in storage.
     *
     * @param \Illuminate\Http\Request $request
     *  
     * @return \Illuminate\Http\Response
     */
    public function store(Request $request)
    {
        return ServiceCategory::create($request);
    }

    /**
     * Display the specified resource.
     *
     * @param \App\ServiceCategory $serviceCategory
     * 
     * @return \Illuminate\Http\Response
     */
    public function show(ServiceCategory $serviceCategory)
    {
        return ServiceCategory::where('id', $serviceCategory->id)->firstOrFail();
    }

    public function find(Request $request)
    {
        return ServiceCategory::where('id', $request->id)->firstOrFail();
    }

    /**
     * Find and Display all recourses that match the serach phrase
     * 
     * @param \Illuminate\Http\Request  $request
     */
    public function search(Request $request)
    {
        $request->validate(
            [
                'phrase' => ['required']
            ]
        );

        $terms = explode(' ', $request->phrase);

        $scs = ServiceCategory::whereHas('tags', function (Builder $query) use ($terms) {
            $query->whereIn('search_tags.term', $terms);
        })->get();

        return $scs;

        /**$sc_ids = ServiceCategoryTag::join(
            'search_tags',
            function ($join) use ($terms) {
                $join->on(
                    'service_category_tags.search_tag_id',
                    '=',
                    'search_tags.id'
                )->whereIn('search_tags.term', $terms);
            }
        )->select('service_category_tags.service_category_id')
            ->distinct('service_category_tags.service_category_id')->get();

        $scs = array();

        foreach ($sc_ids as $id) {
            $sc = ServiceCategory::find($id)->first();

            array_push($scs, $sc);
        }

        return $scs:
        */
    }

    public function fullSearch(Request $request)
    {
        $request->validate(
            [
                'terms' => ['required']
            ]
        );

        $terms = json_decode($request->terms);

        $sub_ids = Subscription::where(function ($query) use ($request, $terms) {
            $query->where('service_category_id', $request->id);
            //foreach ($terms as $term) {
                //$t = json_encode($term);
                //$query->whereJsonCOntains('requirements_provided', $term);
            //}
        })->select('subscriptions.id')->get();

        $subs = array();

        foreach ($sub_ids as $id) {

            $sub = Subscription::with('selling_partner')->find($id)->first();

            $sub_reviews = Shortlist::whereHas('subscription',  function ($query) use ($sub) {
                $query->where('selling_partner_id', $sub->selling_partner_id);
            })->pluck('review');


            $review_counter = 0;
            $star_rating = 0;
            $review_totals = 0;

            if($sub_reviews->count() > 0)
            {
                $review_totals = array_fill(0, count(json_decode($sub_reviews[0])), 0);
                $review_lables = array_fill(0, count(json_decode($sub_reviews[0])), 'Empty');
                $review_counter = 0;

                foreach ($sub_reviews as $review) {
                    //decode review into a parts array
                    if($review && $review != '')
                    {
                        $review = json_decode($review);

                        $count = 0;
                        
                        if(is_array($review))
                        {
                            $count = count($review);
                        }

                        for ($counter = 0; $counter < $count; $counter++) {
                            $part = $review[$counter];

                            $part = json_encode($part);

                            $first_quote = strpos($part, '"') + 1;
                            $last_quote = strrpos($part, '"');
                            $length = $last_quote - $first_quote;

                            $colon_index = strpos($part, ':') + 1;

                            $brace_index = strpos($part, '}');

                            $lable = substr($part, $first_quote, $length);

                            $review_lables[$counter] = $lable;

                            $value = substr($part, $colon_index, $brace_index - $colon_index);

                            $review_totals[$counter] += $value;
                        }
                        $review_counter++;
                    }
                }

                $processed_review = array();


                for ($counter = 0; $counter < count($review_totals); $counter++) {
                    $processed = new stdClass();

                    $processed->label = $review_lables[$counter];
                    $processed->value = $review_totals[$counter] / $review_counter;

                    $star_rating += $processed->value;

                    array_push($processed_review, $processed);
                }

                $sub->chart = $processed_review;
                $sub->star_rating = $star_rating/(count($review_totals) * 10);
            }
            else
            {
                $sub->star_rating = 0;   
            }
            

            $sub->review_count = $review_counter;
            //$sub->star_rating = $star_rating/(count($review_totals) * 10);
        
            //$sub->featureList = array();
            array_push($subs, $sub);
        }

        return $subs;
    }

    /**
     * Update the specified resource in storage.
     *
     * @param  \Illuminate\Http\Request $request
     * @param  \App\ServiceCategory $serviceCategory
     * @return \Illuminate\Http\Response
     */
    public function update(Request $request, ServiceCategory $serviceCategory)
    {
        $inputs = request()->all();
        return ServiceCategory::where('id', $$request->id)
            ->update($inputs);
    }

    /**
     * Remove the specified resource from storage.
     *
     * @param   \App\ServiceCategory  $serviceCategory
     * @return \Illuminate\Http\Response
     */
    public function destroy(ServiceCategory $serviceCategory)
    {
        //
    }
}
