<?php

namespace App\Http\Controllers;

use Illuminate\Http\Request;
use Illuminate\Support\Facades\Http;
use stdClass;

class CompaniesHouseController extends Controller
{
    public function searchCompanies(Request $request)
    {
        $companies_house_url = 'https://api.companieshouse.gov.uk';
        $companies_house_api_key = 'nr7WYPuS1ll7JqQMUwID5YaX2otDZAneSFYkkKoP';

        $request->validate(
            [
                'phrase' => ['required'],
            ]
        );
    

        $response = Http::withBasicAuth($companies_house_api_key, '')->
        get($companies_house_url.'/search/companies', [
            'q' => $request->phrase,
            'items_per_page' => 10, 
            'start_index' => 0
        ]);

        $json = json_decode($response);

        if(property_exists($json, 'total_results') && $json->total_results > 10)
            return "TOO MANY COMPANIES PLEASE TRY SEARCH AGAIN OR USE YOUR COMPANY NUMBER!";

        if(property_exists($json, 'items'))
        {
            $results = array();
            foreach($json->items as $item)
            {
                $res_item = new stdClass();

                $res_item->title = $item->title;
                $res_item->company_number = $item->company_number;

                array_push($results, $res_item);
            }

            return $results;
        }
        
        //500
        return 'ERROR!';
    }

    public function getCompanyInfo(Request $request)
    {
        $companies_house_url = 'https://api.companieshouse.gov.uk';
        $companies_house_api_key = 'nr7WYPuS1ll7JqQMUwID5YaX2otDZAneSFYkkKoP';

        $request->validate(
            [
                'company_number' => ['required'],
            ]
        );

        $response = Http::withBasicAuth($companies_house_api_key, '')->
        get($companies_house_url.'/company//'.$request->company_number);

        //error_log($response);

        $json = json_decode($response);

        $company = new stdClass();

        if(property_exists($json, 'company_number'))
        {
            if($json->company_number == $request->company_number)
                $company->company_number = $json->company_number;
            else
                return "The compnay returned did not match your compnay number!";//500
        }
        else
            return "The compnay returned did not match your compnay number!";//500

        if(property_exists($json, 'company_name'))
            $company->company_name = $json->company_name;

        if(property_exists($json, 'registered_office_address'))
            $company->registered_office_address = $json->registered_office_address;

        if(property_exists($json, 'type'))
            $company->type = $json->type;

        if(property_exists($json, 'annual_return'))
            $company->annual_return = $json->annual_return;

        if(property_exists($json, 'date_of_creation'))
            $company->date_founded = $json->date_of_creation;

            

        return json_encode($company);

        //500
        return 'ERROR!';
    }
            
}
