<?php

namespace App\Http\Controllers;

use App\QwoteRequest;
use App\Shortlist;
use DateTime;
use Illuminate\Http\Request;
use stdClass;

class QwoteRequestController extends Controller
{
    /**
     * Display a listing of the resource.
     *
     * @return \Illuminate\Http\Response
     */
    public function index(Request $request)
    {
        //validate user is office Buyer
        $qwote_requests = QwoteRequest::where('office_buyer_id', $request->user()->office_buyer_id)->with("shortlist.subscription.selling_partner")->with("shortlist.subscription.service_category")->get();

        foreach($qwote_requests as $qwote_request)
        {
            
            $master = $qwote_request->shortlist[0]->subscription->service_category->master_category;
            $qwote_request->master_category = $master;
        }

        

        return $qwote_requests;
    }

    /**
     * Show the form for creating a new resource.
     *
     * @return \Illuminate\Http\Response
     */
    public function create()
    {
        //
    }

    /**
     * Store a newly created resource in storage.
     *
     * @param  \Illuminate\Http\Request  $request
     * @return \Illuminate\Http\Response
     */
    public function store(Request $request)
    {
        $request->validate(
            [
                'requirements' => ['required'],
            ]);

        $request->expires_on = date('Y/m/d', strtotime('+15 days'));
        //$request->expires_on->modify('+14 days');

        do{
            $request->reference = $this->generateRandomString();
            $qr_count = QwoteRequest::where('reference', $request->reference)->get()->count();
        }while($qr_count > 0);


        return QwoteRequest::create(
            [
                'reference' => $request->reference,
                'requirements' => $request->requirements,
                'expires_on' => $request->expires_on,
                'office_buyer_id' => $request->user()->office_buyer_id
            ]
        );
    }

    /**
     * Display the specified resource.
     *
     * @param  \App\QwoteRequest  $qwoteRequest
     * @return \Illuminate\Http\Response
     */
    public function show(Request $request)
    {
        $qwote_request = QwoteRequest::where('id', $request->id)->with('shortlist.subscription.selling_partner')->with('shortlist.subscription.service_category')->firstOrFail();
        foreach($qwote_request->shortlist as $shortlist)
        {
            $shortlist->subscription->selling_partner->chart = $shortlist->subscription->selling_partner->getReviewChart();
            //error_log(json_encode($shortlist->subscription->selling_partner->chart));        
        }
        
        return $qwote_request;
    }

    /**
     * Show the form for editing the specified resource.
     *
     * @param  \App\QwoteRequest  $qwoteRequest
     * @return \Illuminate\Http\Response
     */
    public function edit(QwoteRequest $qwoteRequest)
    {
        //
    }

    /**
     * Update the specified resource in storage.
     *
     * @param  \Illuminate\Http\Request  $request
     * @param  \App\QwoteRequest  $qwoteRequest
     * @return \Illuminate\Http\Response
     */
    public function update(Request $request, QwoteRequest $qwoteRequest)
    {
        $inputs = request()->all();
        return QwoteRequest::where('id', $request->id)
            ->update($$inputs);
    }

    /**
     * Remove the specified resource from storage.
     *
     * @param  \App\QwoteRequest  $qwoteRequest
     * @return \Illuminate\Http\Response
     */
    public function destroy(QwoteRequest $qwoteRequest)
    {
        //
    }

    public static function generateRandomString($length = 6) {
        return substr(str_shuffle(str_repeat($x='0123456789ABCDEFGHIJKLMNOPQRSTUVWXYZ', ceil($length/strlen($x)) )),1,$length);
    }

    private function toChart()
    {
        $chart = new stdClass();

        $chart->value = rand(10,90);;

        $rand = rand(1,4);

        $status = 'OK';

        $status = $rand == 2 ? 'warning' : $status;

        $status = $rand == 3 ? 'severe' : $status;

        $status = $rand == 4 ? 'expired' : $status;

        $chart->status = $status;

        //$chart->message = "SOME CALL TO ACTION";

        return $chart;
    }
}
