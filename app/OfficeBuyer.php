<?php

/**
 * OfficeBuyer File Doc Comment
 *
 * @category File
 * @package  Models
 * @author   Display Name <brad@qwoteme.com>
 */

namespace App;

use Illuminate\Database\Eloquent\Model;

/**
 * OfficeBuyer Class Doc Comment
 *
 * @category Class
 * @package  Models
 * @author   Display Name <brad@qwoteme.com>
 */
class OfficeBuyer extends Model
{
    //
    protected $fillable = [
        'first_name', 'last_name', 'contact', 'office_address',
        'office_postcode', 'number_of_employees', 'turnover_pa',
        'company_house_no', 'company_name'
    ];
}

