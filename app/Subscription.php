<?php

namespace App;

use Illuminate\Database\Eloquent\Model;

class Subscription extends Model
{
    protected $fillable = [
        'selling_partner_id', 'service_category_id', 'requirements_provided','feature_list',
    ];

    public function selling_partner()
    {
        return $this->belongsTo('App\SellingPartner', 'selling_partner_id', 'id');
    }

    public function service_category()
    {
        return $this->belongsTo('App\ServiceCategory', 'service_category_id');
    }
}
