<?php

/**
 * SellingPartner File Doc Comment
 *
 * @category File
 * @package  Models
 * @author   Display Name <brad@qwoteme.com>
 */

namespace App;

use GuzzleHttp\Psr7\Request;
use Illuminate\Database\Eloquent\Model;
use stdClass;

/**
 * SellingPartner Class Doc Comment
 *
 * @category Class
 * @package  Models
 * @author   Display Name <brad@qwoteme.com>
 */
class SellingPartner extends Model
{
    //
    protected $fillable = [
        'company_house_no', 'company_name', 'company_logo', 'carousel_media',
        'company_description', 'head_office_address', 'head_office_postcode',
        'director_name', 'director_contact', 'account_name', 'account_contact',
        'legal_name', 'legal_contact', 'number_of_employees', 'turnover_pa',
        'rep_first_name', 'rep_last_name', 'rep_job_title', 'rep_email',
        'rep_contact', 'verified_partner', 'sponsor_level', 'date_founded'
    ];

    public function getChartAttribute($value)
    {
        return $this->getReviewChart();
    }

    public function getReviewChart ()
    {
        $reviews = Shortlist::whereHas('subscription',  function($query) {
            $query->where('selling_partner_id', $this->id);
        })->with('office_buyer')->get();

        $processed_reviews = array();

        $count = 0;
        $r = json_decode($reviews[0]->review);

        if(is_array($r))
            $count = count($r);

        $review_totals = array_fill(0, $count, 0);
        $review_lables = array_fill(0, $count, 'Empty');
        
        foreach($reviews as $review)
        {
            $review->chart = $this->reviewToChart($review->review);

            $review_total = 0;

            $review_params_count = count($review->chart);

            for ($counter = 0; $counter < $review_params_count; $counter++) {
                $review_lables[$counter] = $review->chart[$counter]->label;
                $review_total += $review->chart[$counter]->value;
                $review_totals[$counter] += $review->chart[$counter]->value;
            }
            
            $review->star_rating = ($review_params_count > 0 ? $review_total/($review_params_count * 10) : 0);

            array_push($processed_reviews, $review);
        }

        $review_counter = count($processed_reviews);

        $star_rating = 0;

        $over_chart = array();

        for ($counter = 0; $counter < count($review_totals); $counter++) {
            $processed = new stdClass();

            $processed->label = $review_lables[$counter];
            $processed->value = $review_totals[$counter] / $review_counter;

            $star_rating += $processed->value;

            array_push($over_chart, $processed);
        }

        $review_over = new stdClass();
        $review_over->review_count = $review_counter;
        $review_over->star_rating = $review_total > 0 ? $star_rating/(count($review_totals) * 10) : 0;

        $review_over->chart = $over_chart;
        //$review_over->reviews = $processed_reviews;

        return $review_over;
    }

    public function reviewToChart($review)
    {
        $json = json_decode($review, true);

        $processed_review = array();

        //error_log(gettype($processed_review));

        //error_log(gettype($json));
        if(is_array($json))
            foreach($json as $part)
            {
                $part = json_encode($part);

                $first_quote = strpos($part, '"') + 1;
                $last_quote = strrpos($part, '"');
                $length = $last_quote - $first_quote;
                
                $colon_index = strpos($part, ':') + 1;
                $brace_index = strpos($part, '}');

                $label = substr($part, $first_quote, $length);

                $value = substr($part, $colon_index, $brace_index - $colon_index);

                $processed = new stdClass();

                $processed->label = $label;
                $processed->value = $value;

                array_push($processed_review, $processed);
            }

    return $processed_review;
    }
}
