<?php

namespace App;

use Illuminate\Database\Eloquent\Model;

class portfolio extends Model
{
    protected $fillable = [
        'title', 'image_url', 'text', 'selling_partner_id'
    ];
}
