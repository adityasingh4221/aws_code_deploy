<?php

/**
 * User File Doc Comment
 *
 * @category File
 * @package  Models
 * @author   Display Name <brad@qwoteme.com>
 */

namespace App;

use Illuminate\Contracts\Auth\MustVerifyEmail;
use Illuminate\Foundation\Auth\User as Authenticatable;
use Illuminate\Notifications\Notifiable;
use Laravel\Passport\HasApiTokens;

/**
 * User Class Doc Comment
 *
 * @category Class
 * @package  Models
 * @author   Display Name <brad@qwoteme.com>
 */
class User extends Authenticatable implements MustVerifyEmail
{
    use HasApiTokens, Notifiable;

    /**
     * The attributes that are mass assignable.
     *
     * @var array
     */
    protected $fillable = [
        'name', 'email', 'password', 'office_buyer_id', 'selling_partner_id',
        'account_type', 'active', 'activation_token'
    ];

    /**
     * The attributes that should be hidden for arrays.
     *
     * @var array
     */
    protected $hidden = [
        'password', 'remember_token', 'activation_token'
    ];

    /**
     * The attributes that should be cast to native types.
     *
     * @var array
     */
    protected $casts = [
        'email_verified_at' => 'datetime',
    ];

    public function office_buyer()
    {
        return $this->belongsTo('App\OfficeBuyer', 'office_buyer_id', 'id');
    }

    public function selling_partner()
    {
        return $this->belongsTo('App\SellingPartner', 'selling_partner_id', 'id');
    }

    public function sendPasswordResetNotification($token)
    {
        $this->notify(new \App\Notifications\MailResetPasswordNotification($token));
    }
}
