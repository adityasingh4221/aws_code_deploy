<?php

namespace App;

use Illuminate\Database\Eloquent\Model;

class Shortlist extends Model
{
    protected $fillable = [
        'subscription_id', 'office_buyer_id', 'review', 'review_text', 'survey_responses', 'status', 'qwote_request_id',
    ];

    protected $hidden = [
        'review',
    ];

    public function office_buyer()
    {
        return $this->belongsTo('App\OfficeBuyer');
    }

    public function subscription()
    {
        return $this->belongsTo('App\Subscription');
    }
}
