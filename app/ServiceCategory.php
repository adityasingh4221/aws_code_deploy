<?php

/**
 * ServiceCategory File Doc Comment
 *
 * @category File
 * @package  Models
 * @author   Display Name <brad@qwoteme.com>
 */

namespace App;

use Illuminate\Database\Eloquent\Model;
use Illuminate\Database\Eloquent\Relations\BelongsToMany;

/**
 * ServiceCategory Class Doc Comment
 *
 * @category Class
 * @package  Models
 * @author   Display Name <brad@qwoteme.com>
 */
class ServiceCategory extends Model
{
    protected $fillabale = [
        'service_name', 'service_description', 'requirement_questions',
    ];

    public function tags()
    {
        return $this->belongsToMany('App\SearchTag', 'service_category_tags');
    }
}
