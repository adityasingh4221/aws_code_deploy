<?php

namespace App;

use Illuminate\Database\Eloquent\Model;

class SearchTag extends Model
{
    protected $fillable = [
        'term'
    ];

    public function serviceCategories()
    {
        return $this->belongsToMany('App\Models\ServiceCategory', 'service_category_tags', 'search_tag_id', 'service_category_id',);
    }
}
