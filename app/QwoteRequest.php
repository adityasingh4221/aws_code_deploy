<?php

namespace App;

use Illuminate\Database\Eloquent\Model;
use stdClass;

class QwoteRequest extends Model
{
    protected $fillable = [
        'expires_on', 'requirements', 'survey_form', 'reference', 'office_buyer_id',
    ];

    //protected $attributes = [
        //'chart' => $this->getChart(),
    //];

    protected $appends = ['chart'];

    public function shortlist()
    {
        return $this->hasMany('App\Shortlist');
    }

    public function getChartAttribute($value)
    {
        return $this->getChart();
        //return "{}";
    }

    private function getChart() 
    {
        $shortlist = $this->shortlist()->get();
        $chart = new stdClass();
        $chart->status = 'OK';
        $chart->value = 0.0;
        $chart->count = 0;
        $chart->ok_count = 0;
        $chart->message = 'All good!';
        
        $count = count($shortlist);
        $chartSliceSize = 100.0/($count==0?1:$count);

        

        foreach($shortlist as $listEntry)
        {
            $subscription = $listEntry->subscription()->get();
            $chart->count++;
            switch($listEntry->status)
            {
                case 'pending':
                    $chart->value += 0.25 * $chartSliceSize;
                    $chart->ok_count++;
                break;
                case 'requested':
                    $chart->value += 0.5 * $chartSliceSize;
                    $chart->ok_count++;
                break;
                case 'complete':
                    $chart->value += 1 * $chartSliceSize;
                    $chart->ok_count++;
                break;
                default:
                    $chart->value += 0.1;
            }
        }

        if($chart->ok_count < 4)
        {
            $chart->status = 'warning';
            $chart->message = 'Low number of active Qwotes! Do you want to shortlist more Partners?';
        }

        if(strtotime($this->expires_on) <= strtotime('-5 days'))
        {
            $chart->status = 'warning';
            $chart->message = 'Qwote Request expires soon! Would you like to extend your Qwote Request?';
        }

        if($chart->ok_count < 1)
        {
            $chart->status = 'severe';
            $chart->message = 'No active Qwotes! Please shortlist more Partners?';
        }
        if(strtotime($this->expires_on) <= strtotime('-2 days'))
        {
            $chart->status = 'severe';
            $chart->message = 'Qwote Request expires in 48 hours! Would you like to extend your Qwote Request?';
        }

        if(strtotime($this->expires_on) <= strtotime('now'))
        {
            if(strtotime($this->expires_on) <= strtotime('now'))
                {
                    $chart->message = 'Qwote Request has expired! Would you like to restart this Qwote Request?';
                }
        }

        return $chart;
    }


}
