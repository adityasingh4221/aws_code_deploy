<?php

namespace App;

use Illuminate\Database\Eloquent\Model;

class ChatMessage extends Model
{

    protected $fillable = ['user_id','chat_id','text', 'time_seen'];

    public function user()
    {
        return $this->belongsTo('App\User');
    }

    public function getSeenAttribute($value)
    {
        return $this->wasSeen();
    }

    public function wasSeen(){
        return $this->time_seen ? true : false;
    }
}
