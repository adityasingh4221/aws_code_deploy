<?php

/**
 * CreateOfficeBuyersTable File Doc Comment
 *
 * @category File
 * @package  Migrations
 * @author   Display Name <brad@qwoteme.com>
 */

use Illuminate\Database\Migrations\Migration;
use Illuminate\Database\Schema\Blueprint;
use Illuminate\Support\Facades\Schema;

/**
 * CreateOfficeBuyersTable Class Doc Comment
 *
 * @category Class
 * @package  Migrations
 * @author   Display Name <brad@qwoteme.com>
 */
class CreateOfficeBuyersTable extends Migration
{
    /**
     * Run the migrations.
     *
     * @return void
     */
    public function up()
    {
        Schema::create(
            'office_buyers',
            function (Blueprint $table) {
                $table->id();
                $table->string('first_name');
                $table->string('last_name')->nullable()->default(null);
                $table->string('contact');
                $table->string('office_address');
                $table->string('office_postcode')->nullable()->default(null);
                $table->integer('number_of_employees')->nullable()->default(null);
                $table->double('turnover_pa', 15, 8)->nullable()->default(null);
                $table->timestamps();

                //Maybes
                /**
                 *  $table->string('company_house_no')->unique();
                 *  $table->string('company_name')->unique();
                 * 
                 */
            }
        );
    }

    /**
     * Reverse the migrations.
     *
     * @return void
     */
    public function down()
    {
        Schema::dropIfExists('office_buyers');
    }
}
