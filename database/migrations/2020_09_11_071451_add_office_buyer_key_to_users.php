<?php

/**
 * AddOfficeBuyerKeyToUsers File Doc Comment
 *
 * @category File
 * @package  Migrations
 * @author   Display Name <brad@qwoteme.com>
 */

use Illuminate\Database\Migrations\Migration;
use Illuminate\Database\Schema\Blueprint;
use Illuminate\Support\Facades\Schema;

/**
 * AddOfficeBuyerKeyToUsers Class Doc Comment
 *
 * @category Class
 * @package  Migrations
 * @author   Display Name <brad@qwoteme.com>
 */
class AddOfficeBuyerKeyToUsers extends Migration
{
    /**
     * Run the migrations.
     *
     * @return void
     */
    public function up()
    {
        Schema::table(
            'users',
            function (Blueprint $table) {
                $table->unsignedBigInteger('office_buyer_id')->nullable()->unique();

                $table->index('office_buyer_id');

                $table->foreign('office_buyer_id')->references('id')->on('office_buyers')->onDelete('cascade');
            }
        );
    }

    /**
     * Reverse the migrations.
     *
     * @return void
     */
    public function down()
    {
        Schema::table(
            'users',
            function (Blueprint $table) {
                $table->dropForeign('users_office_buyer_id_foreign');
                $table->dropIndex('users_office_buyer_id_index');
                $table->dropColumn('office_buyer_id');
            }
        );
    }
}
