<?php

/**
 * AddSellingPartnerKeyToUsers File Doc Comment
 *
 * @category File
 * @package  Migrations
 * @author   Display Name <brad@qwoteme.com>
 */

use Illuminate\Database\Migrations\Migration;
use Illuminate\Database\Schema\Blueprint;
use Illuminate\Support\Facades\Schema;

/**
 * AddSellingPartnerKeyToUsers Class Doc Comment
 *
 * @category Class
 * @package  Migrations
 * @author   Display Name <brad@qwoteme.com>
 */
class AddSellingPartnerKeyToUsers extends Migration
{
    /**
     * Run the migrations.
     *
     * @return void
     */
    public function up()
    {
        Schema::table(
            'users',
            function (Blueprint $table) {
                $table->unsignedBigInteger('selling_partner_id')->nullable()->unique();

                $table->index('selling_partner_id');

                $table->foreign('selling_partner_id')->references('id')->on('selling_partners')->onDelete('cascade');

                $table->string('account_type')->default('office_buyer');
            }
        );
    }

    /**
     * Reverse the migrations.
     *
     * @return void
     */
    public function down()
    {
        Schema::table(
            'users',
            function (Blueprint $table) {
                $table->dropForeign('users_selling_partner_id_foreign');
                $table->dropIndex('users_selling_partner_id_index');
                $table->dropColumn('selling_partner_id');
                $table->dropColumn('account_type');
            }
        );
    }
}
