<?php

use Illuminate\Database\Migrations\Migration;
use Illuminate\Database\Schema\Blueprint;
use Illuminate\Support\Facades\Schema;

class AddCompanyToOfficeBuyers extends Migration
{
    /**
     * Run the migrations.
     *
     * @return void
     */
    public function up()
    {
        Schema::table('office_buyers', function (Blueprint $table) {
            $table->string('company_house_no')->nullable()->unique()->default(null);
            $table->string('company_name')->nullable()->unique()->default(null);;
        });
    }

    /**
     * Reverse the migrations.
     *
     * @return void
     */
    public function down()
    {
        Schema::table('office_buyers', function (Blueprint $table) {
            $table->dropColumn('company_house_no')->unique();
            $table->dropColumn('company_name')->unique();
        });
    }
}
