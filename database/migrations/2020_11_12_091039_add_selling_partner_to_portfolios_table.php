<?php

use Illuminate\Database\Migrations\Migration;
use Illuminate\Database\Schema\Blueprint;
use Illuminate\Support\Facades\Schema;

class AddSellingPartnerToPortfoliosTable extends Migration
{
    /**
     * Run the migrations.
     *
     * @return void
     */
    public function up()
    {
        Schema::table('portfolios', function (Blueprint $table) {
            $table->unsignedBigInteger('selling_partner_id');
            $table->index('selling_partner_id');
            $table->foreign('selling_partner_id')->references('id')->on('selling_partners')->onDelete('cascade');
        });
    }

    /**
     * Reverse the migrations.
     *
     * @return void
     */
    public function down()
    {
        Schema::table('portfolios', function (Blueprint $table) {
            $table->dropForeign('portfolios_selling_partner_id_foreign');
            $table->dropIndex('portfolios_selling_partner_id_index');
            $table->dropColumn('selling_partner_id');
        });

        
    }
}
