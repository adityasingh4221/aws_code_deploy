<?php

/**
 * CreateServiceCategoriesTable File Doc Comment
 *
 * @category File
 * @package  Migrations
 * @author   Display Name <brad@qwoteme.com>
 */

use Illuminate\Database\Migrations\Migration;
use Illuminate\Database\Schema\Blueprint;
use Illuminate\Support\Facades\Schema;

/**
 * CreateServiceCategoriesTable Class Doc Comment
 *
 * @category Class
 * @package  Migrations
 * @author   Display Name <brad@qwoteme.com>
 */
class CreateServicecategoriesTable extends Migration
{
    /**
     * Run the migrations.
     *
     * @return void
     */
    public function up()
    {
        Schema::create(
            'service_categories',
            function (Blueprint $table) {
                $table->id();
                $table->string('service_name');
                $table->longText('service_description');
                $table->longText('service_image')->nullable()->default(null);;
                $table->string('master_category')->nullable()->default(null);;
                $table->json('requirement_questions')->nullable()->default(null);
                $table->timestamps();
            }
        );
    }

    /**
     * Reverse the migrations.
     *
     * @return void
     */
    public function down()
    {
        Schema::dropIfExists('service_categories');
    }
}
