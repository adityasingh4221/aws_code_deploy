<?php

use Illuminate\Database\Migrations\Migration;
use Illuminate\Database\Schema\Blueprint;
use Illuminate\Support\Facades\Schema;

class CreateQwoteRequestsTable extends Migration
{
    /**
     * Run the migrations.
     *
     * @return void
     */
    public function up()
    {
        Schema::create('qwote_requests', function (Blueprint $table) {
            $table->id();
            $table->uuid('reference')->unique();
            $table->date('expires_on');
            $table->json('requirements');
            $table->json('survey_form')->nullable()->default(null);

            $table->unsignedBigInteger('office_buyer_id');
            $table->index('office_buyer_id');
            $table->foreign('office_buyer_id')->references('id')->on('office_buyers')->onDelete('cascade');

            $table->timestamps();
        });
    }

    /**
     * Reverse the migrations.
     *
     * @return void
     */
    public function down()
    {
        Schema::dropIfExists('qwote_requests');
    }
}
