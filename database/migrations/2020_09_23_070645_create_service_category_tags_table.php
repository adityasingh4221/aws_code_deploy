<?php

use Illuminate\Database\Migrations\Migration;
use Illuminate\Database\Schema\Blueprint;
use Illuminate\Support\Facades\Schema;

class CreateServiceCategoryTagsTable extends Migration
{
    /**
     * Run the migrations.
     *
     * @return void
     */
    public function up()
    {
        Schema::create(
            'search_tags',
            function (Blueprint $table) {
                $table->id();
                $table->string('term')->unique();
                $table->timestamps();
            }
        );

        Schema::create(
            'service_category_tags',
            function (Blueprint $table) {
                $table->id();

                $table->unsignedBigInteger('search_tag_id');
                $table->index('search_tag_id');
                $table->foreign('search_tag_id')->references('id')->on('search_tags')->onDelete('cascade');

                $table->unsignedBigInteger('service_category_id');
                $table->index('service_category_id');
                $table->foreign('service_category_id')->references('id')->on('service_categories')->onDelete('cascade');

                $table->timestamps();
            }
        );
    }

    /**
     * Reverse the migrations.
     *
     * @return void
     */
    public function down()
    {
        Schema::dropIfExists('service_category_tags');

        Schema::dropIfExists('search_tags');
    }
}
