<?php

use Illuminate\Database\Migrations\Migration;
use Illuminate\Database\Schema\Blueprint;
use Illuminate\Support\Facades\Schema;

class AddQwoteRequestsToShortlistTable extends Migration
{
    /**
     * Run the migrations.
     *
     * @return void
     */
    public function up()
    {
        Schema::table('shortlists', function (Blueprint $table) {
            $table->unsignedBigInteger('qwote_request_id');
            $table->index('qwote_request_id');
            $table->foreign('qwote_request_id')->references('id')->on('qwote_requests')->onDelete('cascade');
        });
    }

    /**
     * Reverse the migrations.
     *
     * @return void
     */
    public function down()
    {
        Schema::table('shortlists', function (Blueprint $table) {
            $table->dropForeign('shortlists_qwote_request_id_foreign');
            $table->dropIndex('shortlists_qwote_request_id_index');
            $table->dropColumn('qwote_request_id');
        });
    }
}
