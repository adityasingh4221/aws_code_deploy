<?php

/**
 * CreateSellingPartnersTable File Doc Comment
 *
 * @category File
 * @package  Migrations
 * @author   Display Name <brad@qwoteme.com>
 */

use Illuminate\Database\Migrations\Migration;
use Illuminate\Database\Schema\Blueprint;
use Illuminate\Support\Facades\Schema;
use phpDocumentor\Reflection\Types\Nullable;

/**
 * CreateSellingPartnersTable Class Doc Comment
 *
 * @category Class
 * @package  Migrations
 * @author   Display Name <brad@qwoteme.com>
 */
class CreateSellingPartnersTable extends Migration
{
    /**
     * Run the migrations.
     *
     * @return void
     */
    public function up()
    {
        Schema::create(
            'selling_partners',
            function (Blueprint $table) {
                $table->id();
                $table->string('company_house_no')->unique();
                $table->string('company_name')->unique();
                $table->string('company_logo');
                $table->string('carousel_media')->nullable()->default(null);
                $table->longText('company_description');
                $table->string('head_office_address');
                $table->string('head_office_postcode')->nullable()->default(null);
                $table->string('director_name')->nullable()->default(null);
                $table->string('director_contact')->nullable()->default(null);
                $table->string('account_name')->nullable()->default(null);
                $table->string('account_contact')->nullable()->default(null);
                $table->string('legal_name')->nullable()->default(null);
                $table->string('legal_contact')->nullable()->default(null);
                $table->integer('number_of_employees')->nullable()->default(null);
                $table->double('turnover_pa', 15, 8)->nullable()->default(null);
                $table->string('rep_first_name')->nullable()->default(null);
                $table->string('rep_last_name')->nullable()->default(null);
                $table->string('rep_job_title')->nullable()->default(null);
                $table->string('rep_email')->nullable()->default(null);
                $table->string('rep_contact')->nullable()->default(null);
                $table->boolean('verified_partner')->default(false);
                $table->integer('sponsor_level')->default(0);
                $table->timestamps();
            }
        );
    }

    /**
     * Reverse the migrations.
     *
     * @return void
     */
    public function down()
    {
        Schema::dropIfExists('selling_partners');
    }
}
