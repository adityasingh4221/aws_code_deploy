<?php

use App\SearchTag;
use Illuminate\Database\Seeder;

class SearchTagSeeder extends Seeder
{
    /**
     * Run the database seeds.
     *
     * @return void
     */
    public function run()
    {
        SearchTag::create(['term'=> "accountants"]);
        SearchTag::create(['term'=> "finance"]);
        SearchTag::create(['term'=> "logo"]);
        SearchTag::create(['term'=> "design"]);
        SearchTag::create(['term'=> "graphic"]);
        SearchTag::create(['term'=> "marketing"]);
        SearchTag::create(['term'=> "web"]);
        SearchTag::create(['term'=> "google"]);
        SearchTag::create(['term'=> "SEO"]);
        SearchTag::create(['term'=> "search"]);
        SearchTag::create(['term'=> "develop"]);
        SearchTag::create(['term'=> "program"]);
        SearchTag::create(['term'=> "code"]);
        SearchTag::create(['term'=> "coding"]);
        SearchTag::create(['term'=> "telephone"]);
        SearchTag::create(['term'=> "telephony"]);
        SearchTag::create(['term'=> "telemarketing"]);

    }
}
