<?php

use App\OfficeBuyer;
use App\QwoteRequest;
use App\Shortlist;
use App\Subscription;
use Illuminate\Database\Seeder;

class ShortlistSeeder extends Seeder
{
    /**
     * Run the database seeds.
     *
     * @return void
     */
    public function run()
    {
        $qwoteRequests = QwoteRequest::all();
        foreach ($qwoteRequests as $qwoteRequest) {
            $randSubs = Subscription::inRandomOrder()->limit(4)->get();
            $rand = rand(1,4);

            $status = 'pending';

            $status = $rand == 2 ? 'requested' : $status;

            $status = $rand == 3 ? 'complete' : $status;

            $status = $rand == 4 ? 'rejected' : $status;
            
            foreach ($randSubs as $sub) {
                Shortlist::create(
                    [
                        'qwote_request_id' => $qwoteRequest->id,
                        'subscription_id' => $sub->id,
                        'review' => '[{"Value for Money": '. rand(0,50) .'}, {"Quality of Service": '. rand(0,50) .'}, {"Turn Around Time" : '. rand(0,50) .'}, {"Would you refer": '. rand(0,50) .'}, {"Customer Experience": '. rand(0,50) .'}]',
                        'review_text' => 'Lorem ipsum dolor sit amet, consectetur adipiscing elit, sed do eiusmod tempor incididunt ut labore et dolore magna aliqua. Ac felis donec et odio pellentesque diam volutpat commodo sed. Donec enim diam vulputate ut pharetra sit amet aliquam. Orci phasellus egestas tellus rutrum tellus. Diam quam nulla porttitor massa. Hendrerit gravida rutrum quisque non tellus. Cras adipiscing enim eu turpis egestas pretium. Tincidunt ornare massa eget egestas purus viverra accumsan. Sapien nec sagittis aliquam malesuada bibendum. Libero id faucibus nisl tincidunt eget nullam non nisi.',
                        'survey_responses' => '{"budget":"do keep me","start_date":"12/4/2020","end_date":"12/24/2020"}',
                        'review_timestamp' => new DateTime(),

                        'status' => $status,
                    ]
                );
            }
        }
    }
}
