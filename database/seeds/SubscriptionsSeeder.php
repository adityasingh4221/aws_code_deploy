<?php

use App\SellingPartner;
use App\ServiceCategory;
use App\Subscription;
use Illuminate\Database\Seeder;

class SubscriptionsSeeder extends Seeder
{
    /**
     * Run the database seeds.
     *
     * @return void
     */
    public function run()
    {
        $sellingPartners = SellingPartner::all();
        foreach ($sellingPartners as $sellingPartner) {
            $randServices = ServiceCategory::inRandomOrder()->limit(5)->get();
            foreach ($randServices as $service) {
                Subscription::create(
                    [
                        'selling_partner_id' => $sellingPartner->id,
                        'service_category_id' => $service->id,
                        'requirements_provided' => '{"budget":"do keep me","start_date":"12/4/2020","end_date":"12/24/2020"}',
                    ]
                );
            }
        }
    }
}
