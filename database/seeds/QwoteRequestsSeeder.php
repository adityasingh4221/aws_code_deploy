<?php

use App\Http\Controllers\QwoteRequestController;
use App\OfficeBuyer;
use App\QwoteRequest;
use Illuminate\Database\Seeder;

class QwoteRequestsSeeder extends Seeder
{
    /**
     * Run the database seeds.
     *
     * @return void
     */
    public function run()
    {
        $office_buyers = OfficeBuyer::all();

        foreach($office_buyers as $office_buyer)
        {
            for($i = 0; $i < 4 ;$i++)
            {
                $expires_on = date('Y/m/d', strtotime('+15 days'));
                //$request->expires_on->modify('+14 days');

                $refernce = "";
                do{
                    $reference = QwoteRequestController::generateRandomString();
                    $qr_count = QwoteRequest::where('reference', $reference)->get()->count();
                    error_log($qr_count . " qwotes found!");
                }while($qr_count > 0);

                error_log("New Qwote Created: " . $reference);

                QwoteRequest::create(
                    [
                        'reference' => $reference,
                        'requirements' => '{"budget":"do keep me","start_date":"12/4/2020","end_date":"12/24/2020"}',
                        'expires_on' => $expires_on,
                        'office_buyer_id' => $office_buyer->id
                    ]
                );
            }
        }
    }
}
