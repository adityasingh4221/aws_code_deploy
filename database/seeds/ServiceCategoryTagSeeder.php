<?php

use App\SearchTag;
use App\ServiceCategory;
use App\ServiceCategoryTag;
use Illuminate\Database\Seeder;

class ServiceCategoryTagSeeder extends Seeder
{
    /**
     * Run the database seeds.
     *
     * @return void
     */
    public function run()
    {
        $serviceCategories = ServiceCategory::all();
        foreach ($serviceCategories as $serviceCategory) {
            $randTags = SearchTag::inRandomOrder()->limit(10)->get();
            foreach ($randTags as $tag) {
                ServiceCategoryTag::create(
                    [
                        'search_tag_id' => $tag->id,
                        'service_category_id' => $serviceCategory->id,
                    ]
                );
            }
        }
    }
}
