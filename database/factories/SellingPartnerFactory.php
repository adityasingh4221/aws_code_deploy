<?php

/**
 * SellingPartnerFactory File Doc Comment
 *
 * @category File
 * @package  Factories
 * @author   Display Name <brad@qwoteme.com>
 * @var      \Illuminate\Database\Eloquent\Factory $factory
 */

use App\SellingPartner;
use Faker\Generator as Faker;

$factory->define(
    SellingPartner::class,
    function (Faker $faker) {
        return [
            'company_house_no' => $faker->company,
            'company_name' => $faker->company,
            'company_logo' => $faker->company,
            'carousel_media' => $faker->company,
            'company_description' => $faker->paragraph(),
            'head_office_address' => $faker->address,
            'director_name' => $faker->name(),
            'director_contact' => $faker->phoneNumber,
            'account_name' => $faker->name(),
            'account_contact' => $faker->phoneNumber,
            'legal_name' => $faker->name(),
            'legal_contact' => $faker->phoneNumber,
            'number_of_employees' => $faker->numberBetween(100, 1000),
            'turnover_pa' => $faker->numberBetween(100000, 10000000),
            'rep_first_name' => $faker->firstName(),
            'rep_last_name' => $faker->lastName,
            'rep_job_title' => $faker->jobTitle,
            'rep_email' => $faker->email,
            'rep_contact' => $faker->phoneNumber,
            'verified_partner' => $faker->boolean(),
            //'sponsor_level'
            //'head_office_postcode'=>$faker
        ];
    }
);
