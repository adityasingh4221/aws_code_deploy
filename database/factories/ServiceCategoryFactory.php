<?php

/** @var \Illuminate\Database\Eloquent\Factory $factory */

use App\ServiceCategory;
use Faker\Generator as Faker;

$factory->define(
    ServiceCategory::class,
    function (Faker $faker) {
        return [
            'service_name' => $faker->domainWord,
            'service_description' => $faker->paragraph(),
            'requirement_questions' => '[{"initialValues":{},"fields":[{"type":"radio","label":"Do you wish to pay monthly or annually?","name":["monthly_annually"],"options":[{"label":"Monthly","value":"0"},{"label":"Annually","value":"1"}]},{"type":"text","label":"What is your monthly/annual budget?","name":["budget"],"options":[]},{"type":"date","label":"When would you like the service to start?","name":["start_date"],"options":[]},{"type":"date","label":"When would you like the service to end?","name":["end_date"],"options":[]},{"type":"radio","label":"Are you the final decision maker?","name":["decision_maker"],"options":[{"label":"Yes","value":"0"},{"label":"No","value":"1"}],"slave":[{"masterName":"decision_maker","slaveName":"other_decision_maker","onMasterVal":"1"}]},{"type":"text","label":"Other decision maker","name":["other_decision_maker"],"options":[],"validate":["required"]},{"type":"radio","label":"Purchase or lease","name":["purchase_lease"],"options":[{"label":"Purchase","value":"0"},{"label":"Lease","value":"1"},{"label":"Unsure, please advise","value":"2"}]}],"buttons":[{"label":"Submit","variant":"contained","color":"primary","type":"submit","disabled":["submitting"],"onClick":null}]},{"initialValues":{},"fields":[{"type":"radio","label":"Ink","name":["ink"],"options":[{"label":"Colour","value":"0"},{"label":"Black & white","value":"1"},{"label":"Both","value":"2"}]},{"type":"checkbox","label":"Paper size","name":["paper_a3","paper_a4","paper_a5","paper_a1","paper_other"],"options":[{"label":"A3"},{"label":"A4"},{"label":"A5"},{"label":"A1"},{"label":"Other"}],"slave":[{"masterName":"paper_other","slaveName":"other_paper_size","onMasterVal":true}]},{"type":"text","label":"Other paper size","name":["other_paper_size"],"options":[],"validate":["required"]},{"type":"checkbox","label":"Device type","name":["device_multi","device_printer","device_photocopier","device_scanner","device_fax","device_other"],"options":[{"label":"Multi-function (Printer/Scanner/Copier/Fax)"},{"label":"Desktop printer"},{"label":"Photocopier (only)"},{"label":"Scanner"},{"label":"Fax machine"},{"label":"Other"}],"slave":[{"masterName":"device_other","slaveName":"other_device","onMasterVal":true}]},{"type":"text","label":"Other device","name":["other_device"],"options":[],"validate":["required"]},{"type":"comment","label":"Briefly describe your requirements","name":["describe_requirements"],"options":[]}],"buttons":[{"label":"Previous","variant":"outlined","color":"primary","type":null,"disabled":[],"onClick":"previous"},{"label":"Submit","variant":"contained","color":"primary","type":"submit","disabled":["submitting"],"onClick":null}]}]',
        ];
    }
);
