<?php

/**
 * SellingPartnerFactory File Doc Comment
 *
 * @category File
 * @package  Factories
 * @author   Display Name <brad@qwoteme.com>
 * @var      \Illuminate\Database\Eloquent\Factory $factory
 */

use App\OfficeBuyer;
use App\User;
use Faker\Generator as Faker;
use Illuminate\Support\Str;

$factory->define(
    User::class,
    function (Faker $faker) {
        $OfficeBuyer = OfficeBuyer::inRandomOrder()->first();

        $value = rand(0, 1) == 1;

        if ($value) {
            return [
                'name' => $faker->name,
                'email' => $faker->unique()->safeEmail,
                'email_verified_at' => now(),
                'password' => '$2y$10$92IXUNpkjO0rOQ5byMi.Ye4oKoEa3Ro9llC/.og/at2.uheWG/igi', // password
                'activation_token' => Str::random(60),
                'remember_token' => Str::random(10),
                'office_buyer_id' => function () {
                    return factory(\App\OfficeBuyer::class)->create()->id;
                },
                'account_type' => 'office_buyer',
            ];
        } else {
            return [
                'name' => $faker->name,
                'email' => $faker->unique()->safeEmail,
                'email_verified_at' => now(),
                'password' => '$2y$10$92IXUNpkjO0rOQ5byMi.Ye4oKoEa3Ro9llC/.og/at2.uheWG/igi', // password
                'activation_token' => Str::random(60),
                'remember_token' => Str::random(10),
                'selling_partner_id' => function () {
                    return factory(\App\SellingPartner::class)->create()->id;
                },
                'account_type' => 'selling_partner',
            ];
        }
    }
);
