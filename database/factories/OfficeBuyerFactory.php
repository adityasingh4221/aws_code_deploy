<?php

/**
 * OfficeBuyerFactory File Doc Comment
 *
 * @category File
 * @package  Factories
 * @author   Display Name <brad@qwoteme.com>
 * @var      \Illuminate\Database\Eloquent\Factory $factory
 */

use App\OfficeBuyer;
use Faker\Generator as Faker;

$factory->define(
    OfficeBuyer::class,
    function (Faker $faker) {
        return [
            'first_name' => $faker->firstName(),
            'last_name' => $faker->lastName,
            'contact' => $faker->phoneNumber,
            'office_address' => $faker->address,
            'number_of_employees' => $faker->numberBetween(100, 500),
            'turnover_pa' => $faker->numberBetween(100000, 10000000),
            'company_house_no' => $faker->regexify('[A-Za-z0-9]{16}'),
            'company_name' => $faker->company,
        ];
    }
);
