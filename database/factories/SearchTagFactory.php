<?php

/** @var \Illuminate\Database\Eloquent\Factory $factory */

use App\SearchTag;
use Faker\Generator as Faker;

$factory->define(
    SearchTag::class,
    function (Faker $faker) {
        return [
            'term' => $faker->unique()->word(),
        ];
    }
);
